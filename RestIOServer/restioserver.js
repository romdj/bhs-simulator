'use strict';
const Hapi = require('hapi');
const Inert = require('inert');
const Vision = require('vision');
const HapiSwagger = require('hapi-swagger');
const Pack = require('./package');
const Stomp = require('stomp-client');
const fs = require('fs');
const http = require('http');
const _ = require('lodash');
const apsbParams = require('./config/apsbParams.json');
let serverConfig = require('./config/restioserver.config.json');
let kioskInfo = require('./config/kioskInfoData.json');
var pubsubID = 0;

// FUNCTION REGISTER REMPLAÇÉE PAR FICHIER PROPERTIES (FICHIER JSON)
let locale = 'fr';
let messageList = (locale == 'fr') ? require('./config/errorCode.fr.json') : (locale == 'en') ? require('./config/errorCode.en.json') : null;

// let hwhTopics = [];

function getErrorMessage(errorCode) {
  return messageList[errorCode];
}

let observers = [];
let portObservers = 4000;
let pathNotify = '/notifyError';
let errAMQC = false;

const restServer = Hapi.server(serverConfig.serverConf);

restServer.route({
  method: 'GET',
  path: '/hello',
  config: {
    description: 'Say Hello, get World',
    notes: 'Returns World when the Hello Route is called [testing purposes]',
    tags: ['api', 'test'],
  },
  handler: function (request, h) {
    registerIP(request.info.remoteAddress);
    return 'World!';
  }
});

restServer.route({
  method: 'GET',
  path: '/REGISTER',
  handler: function (request, h) {
    registerIP(request.info.remoteAddress);
    return 'REGISTERING FOR KIOSK ' /*+ request.params.kiosk_id*/ + 'WITH URL: ';
  },
  config: {
    description: 'Kiosk Registration',
    notes: 'Adds a Kiosk to the list of the registered IPs',
    tags: ['api'],
  },
});

restServer.route({
  method: 'GET',
  path: '/BHSCMD/GETERRORLIST',
  handler: function (request, h) {
    return messageList;
  },
  config: {
    cors: true,
    description: 'Getting All the Error List',
    notes: 'Getting All the Error List for all Kiosks',
    tags: ['api', 'plc', 'info'],
  }
});

restServer.route({
  method: 'GET',
  path: '/BHSCMD/GETKIOSKLIST',
  config: {
    cors: true,
    description: 'Getting Kiosk List',
    notes: 'Getting the list of Kiosks registered in the API',
    tags: ['api', 'plc', 'info'],
  },
  handler: function (request, h) {
    registerIP(request.info.remoteAddress);
    let result = {
      kiosks: []
    };
    for (let kiosk of kioskInfo.kiosks) {
      result.kiosks.push({
        kioskId: kiosk.kioskId,
        alias: kiosk.alias,
        nbrConveyors: kiosk.nbrConveyors,
        scaleConveyor: kiosk.scaleConveyor,
        forwardAndReverseSupported: kiosk.forwardAndReverseSupported
      });
    }
    result.return = {
      returnCode: 0,
      returnMessage: getErrorMessage(0)
    };
    return result;
  }
});

restServer.route({
  method: 'POST',
  path: '/BHSCMD/ADDKIOSK',
  config: {
    description: 'Registering a New Kiosk to the list',
    notes: 'Adding a new Kiosk to the API List',
    tags: ['api', 'plc', 'info'],
  },
  handler: function (request, h) {
    registerIP(request.info.remoteAddress);
    return 'in /BHSCMD/ADDKIOSK';
  }
});

restServer.route({
  method: 'GET',
  path: '/BHSCMD/GETKIOSK/{kiosk_id}',
  config: {
    description: 'Getting All the Kiosk information from a given Kiosk',
    notes: 'Getting All the Kiosk information from a given Kiosk',
    tags: ['api', 'plc', 'info'],
  },
  handler: function (request, h) {
    registerIP(request.info.remoteAddress);
    let tempObj = getOrCreateKiosk(request.params.kiosk_id);
    return tempObj;
  }
});

restServer.route({
  method: 'GET',
  path: '/BHSCMD/GETBHSWEIGHTLIMITS',
  config: {
    cors: true,
    description: 'Getting Weight limits for all Kiosks',
    notes: 'returning list of min and max Weights',
    tags: ['api', 'plc', 'info'],
  },
  handler: function (request, h) {
    registerIP(request.info.remoteAddress);
    let result = {
      kiosks: []
    };
    for (kiosk of kioskInfo.kiosks) {
      result.kiosks.push({
        kioskId: kiosk.kioskId,
        minWeight: kiosk.minWeight,
        maxWeight: kiosk.maxWeight
      });
    }
    result.return = {
      returnCode: 0,
      returnMessage: getErrorMessage(0)
    };
    return result;
  }
});

restServer.route({
  method: 'GET',
  path: '/BHSCMD/GETBHSWEIGHTLIMITS/{kiosk_id}',
  config: {
    cors: true,
    description: 'Getting a given Kiosk Weight limits',
    notes: 'returning min and max Weights',
    tags: ['api', 'plc', 'info'],
  },
  handler: function (request, h) {
    registerIP(request.info.remoteAddress);
    let tempObj = getOrCreateKiosk(request.params.kiosk_id);
    let result = {
      kiosks: [{
        kioskId: tempObj.kioskId,
        minWeight: tempObj.minWeight,
        maxWeight: tempObj.maxWeight
      }]
    };
    result.return = {
      returnCode: tempObj.Error,
      returnMessage: getErrorMessage(tempObj.Error)
    };
    return result;
  }
});

restServer.route({
  method: 'GET',
  path: '/BHSCMD/FORWARD/{kiosk_id}',
  config: {
    cors: true,
    description: 'Activating Forward for a given Kiosk',
    notes: 'Triggering the Forward tag to 1 in the PLC associated to the given Kiosk (only valid for 3 Conveyors configuration)',
    tags: ['api', 'plc', 'commands'],
  },
  handler: function (request, h) {
    registerIP(request.info.remoteAddress);
    sendConveyor(request.params.kiosk_id, 'FORWARD');
    let tempObj = getOrCreateKiosk(request.params.kiosk_id);
    let response = {
      "kioskId": request.params.kiosk_id,
      "command": "FORWARD",
      "resultCode": tempObj.Error
    }
    return response;
  }
});

restServer.route({
  method: 'GET',
  path: '/BHSCMD/REVERSE/{kiosk_id}',
  config: {
    cors: true,
    description: 'Activating reverse for a given Kiosk',
    notes: 'Triggering the Revers tag to 1 in the PLC associated to the given Kiosk (only valid for 3 Conveyors configuration)',
    tags: ['api', 'plc', 'commands'],
  },
  handler: function (request, h) {
    registerIP(request.info.remoteAddress);
    sendConveyor(request.params.kiosk_id, 'REVERSE');
    let tempObj = getOrCreateKiosk(request.params.kiosk_id);
    tempObj.ConveyorReverse = 1;
    let response = {
      "kioskId": request.params.kiosk_id,
      "command": "REVERSE",
      "resultCode": tempObj.Error
    };
    return response;
  }
});

restServer.route({
  method: 'GET',
  path: '/BHSCMD/BACKWARD/{kiosk_id}',
  config: {
    cors: true,
    description: 'Activating Backward (==REVERSE) for a given Kiosk',
    notes: 'Triggering the Backward (==REVERSE) tag to 1 in the PLC associated to the given Kiosk (only valid for 3 Conveyors configuration)',
    tags: ['api', 'plc', 'commands'],
  },
  handler: function (request, h) {
    registerIP(request.info.remoteAddress);
    sendConveyor(request.params.kiosk_id, 'REVERSE');
    let tempObj = getOrCreateKiosk(request.params.kiosk_id);
    tempObj.ConveyorReverse = 1;
    // let response = {
    //   "kioskId": request.params.kiosk_id,
    //   "command": "BACKWARD",
    //   "resultCode": tempObj.Error
    // }
    let response = {
      "kioskId": request.params.kiosk_id,
      "command": "BACKWARD",
      "resultCode": tempObj.Error
    }
    return response;
  }
});

restServer.route({
  method: 'GET',
  path: '/BHSCMD/INJECT/{kiosk_id}',
  config: {
    cors: true,
    description: 'Activating injection for a given Kiosk',
    notes: 'Triggering the Injection tag to 1 in the PLC associated to the given Kiosk',
    tags: ['api', 'plc', 'commands'],
  },
  handler: function (request, h) {
    registerIP(request.info.remoteAddress);
    sendConveyor(request.params.kiosk_id, 'INJECT');
    let tempObj = getOrCreateKiosk(request.params.kiosk_id);
    let response = {
      "kioskId": request.params.kiosk_id,
      "command": "INJECT",
      "resultCode": tempObj.Error
    };
    return response;
  }
});

restServer.route({
  method: 'GET',
  path: '/BHSCMD/REQUESTKIOSKSTATUS/{kiosk_id}',
  config: {
    cors: true,
    description: 'Requesting a given Kiosk Status',
    notes: 'Returning the status of a given Kiosk',
    tags: ['api', 'plc', 'info'],
  },
  handler: function (request, h) {
    registerIP(request.info.remoteAddress);
    let tempObj = getOrCreateKiosk(request.params.kiosk_id);
    return tempObj.Status;
  }
});

restServer.route({
  method: 'GET',
  path: '/BHSCMD/REQUESTKIOSKWEIGHT/{kiosk_id}',
  config: {
    cors: true,
    description: 'Requesting a given Kiosk Weight (g)',
    notes: 'Returning the Weight (g) of a given Kiosk',
    tags: ['api', 'plc', 'info'],
  },
  handler: function (request, h) {
    registerIP(request.info.remoteAddress);
    let tempObj = getOrCreateKiosk(request.params.kiosk_id);
    return tempObj.CurrentWeight;
  }
});

restServer.route({
  method: 'GET',
  path: '/CUSSSBD/BHSSTATUS',
  config: {
    cors: true,
    description: 'Requesting all BHS Status',
    notes: 'Returning the status of all BHS',
    tags: ['api', 'plc', 'info'],
  },
  handler: function (request, h) {
    registerIP(request.info.remoteAddress);
    return 'CUSSSBD BHS STATUS REQUESTED';
  }
});

restServer.route({
  method: 'GET',
  path: '/CUSSSBD/BHSSTATUS/{kiosk_id}',
  config: {
    cors: true,
    description: 'Requesting a given BHS Status',
    notes: 'Returning the status of a given BHS',
    tags: ['api', 'plc', 'info'],
  },
  handler: function (request, h) {
    registerIP(request.info.remoteAddress);
    return 'CUSSSBD BHS STATUS REQUESTED FOR KIOSK ' + request.params.kiosk_id;
  }
});

restServer.route({
  method: 'GET',
  path: '/SETLANGUAGE/{language}',
  config: {
    cors: true,
    description: 'Changing the language of the Alarms text',
    notes: 'Setting the language of the Alarms text (will respond to :EN,FR,ENGLISH,FRANCAIS)',
    tags: ['api', 'config'],
  },
  handler: function (request, h) {
    registerIP(request.info.remoteAddress);
    switch (request.params.language.toLowerCase()) {
      case 'fr':
        locale = 'fr';
        messageList = require('./config/errorCode.fr.json');
        break;
      case 'french':
        locale = 'fr';
        messageList = require('./config/errorCode.fr.json');
        break;
      case 'francais':
        locale = 'fr';
        messageList = require('./config/errorCode.fr.json');
        break;
      case 'en':
        locale = 'en';
        messageList = require('./config/errorCode.en.json');
        break;
      case 'english':
        locale = 'en';
        messageList = require('./config/errorCode.en.json');
        break;
      default:
        locale = 'fr';
        messageList = require('./config/errorCode.fr.json');
        break;
    }
    return 'Changed Error Language to ' + locale;
  }
});

// SIMULATOR COMMANDS (INTERNAL DATA MANIPULATION) == TEMPORARY/TO BE SOON DELETED

function computeBHSData(aKioskId) {
  let kiosk = _.find(kioskInfo.kiosks, function (obj) {
    return obj.kioskId === aKioskId;
  });
  kiosk.Info = (simulatorData[aKioskId].cellConveyor1) ? 1 : 0;
  kiosk.Info += (simulatorData[aKioskId].cellConveyor2) ? 16 : 0;
  kiosk.Info += (simulatorData[aKioskId].intrusion) ? 2 : 0;
  kiosk.Info += (simulatorData[aKioskId].bhsReady) ? 32 : 0;
  kiosk.Status = simulatorData[aKioskId].key ? 1 : 0;
  // kiosk.Error = simulatorData[aKioskId].emergencyStop ? 1 : 0;
}

async function start() {
  const swaggerOptions = {
    info: {
      title: 'Engie PLC Data Interface API Documentation',
      version: Pack.version,
    },
  };

  await restServer.register([
    Inert,
    Vision,
    {
      plugin: HapiSwagger,
      options: swaggerOptions
    }
  ]);

  try {
    await restServer.start();
    console.log('Server running at:', restServer.info.uri);
  } catch (err) {
    console.log(err);
  }
}

function listenScale() {
  const amqcListener = new Stomp(serverConfig.PLCServer.host, serverConfig.PLCServer.port, serverConfig.PLCServer.username, serverConfig.PLCServer.password);
  const topic = serverConfig.scaleIn;
  amqcListener.connect(() => {
      errAMQC = false;
      amqcListener.subscribe(topic, (message, error) => {
        if (error) console.error('error in listenScale: ', error);
        console.log('parsing message: ', message);
        parseScale(message);
      });
    },
    (err) => {
      if (!errAMQC) {
        errAMQC = true;
        console.error('error connecting to AMQC Scale', err);
      }
      setTimeout(listenScale, 5000);
    });
}

function parseScale(message) {
  let kiosk = message.split('/')[1];
  let weight = message.split('/')[2];
  let status = message.split('/')[3];
  let tempObj = getOrCreateKiosk(kiosk);
  if (tempObj) {
    tempObj.CurrentWeight = Number(weight) * 1000;
    tempObj.BHSScaleStatus = status;
  } else {
    console.error('couldn\'t find kiosk ', kiosk);
  }
}

function registerIP(ipObserver) {
  if (!_.find(observers, function (obs) {
      return obs === ipObserver;
    }))
    observers.push(ipObserver);
}

function notifyError(errorCode) {
  observers.forEach((obs) => {
    http.createClient()
    // An object of options to indicate where to post to
    let data = {
      ErrorCode: errorCode,
      ErrorCode: getErrorMessage(errorCode)
    };
    let post_options = {
      host: obs,
      port: portObservers,
      path: pathNotify,
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      }
    };
    // Set up the request
    let post_req = http.request(post_options, function (res) {
      res.setEncoding('utf8');
      res.on('data', function (chunk) {});
    });
    // post the data
    post_req.write(data);
    post_req.end();
  });
}

function sendConveyor(kiosk, command) {
  const topic = serverConfig.conveyorCommandOut;
  nextPubSubID();
  let headers = {
    // Properties required by stompit
    destination: topic,
    'content-type': 'text/plain',
    // Properties required by APSB
    ClientName: apsbParams.apsb.clientName,
    PubSubIDUTC: nextPubSubID(),
    GenerationTimeStamp: new Date().getTime()
  };
  const amqcListener = new Stomp(serverConfig.PLCServer.host, serverConfig.PLCServer.port, serverConfig.PLCServer.username, serverConfig.PLCServer.password);
  amqcListener.connect(() => {
    amqcListener.publish(topic, ['UDROP', kiosk, command].join('/'), headers);
  }, (err) => {
    console.error('error sending Command');
  });
}

/*
 {
    "source": "IER_SBD_003",
    "tags": [{
      "tagName": "Channel1.PLC.IER_SBD_003_ConveyorForward",
      "tagValue": 1,
      "quality": "Good",
      "hasChanged": true,
      "lastUpdatedBy": "OPC Server"
    }]
  }

  HWH Connection -> msg sending :
  const uDropTopic = 'HARDWAREHUB_COMMANDS';
  let message = 'Channel1.PLC.' + airport + '-' + request.params.kiosk_id + '_ConveyorForward=1'
  APSBClient.sendMessage(uDropTopic,message);
*/

function listenConveyorCommands() {
  const amqcListener = new Stomp(serverConfig.PLCServer.host, serverConfig.PLCServer.port, serverConfig.PLCServer.username, serverConfig.PLCServer.password);
  const topic = serverConfig.conveyorCommandIn;
  amqcListener.connect(() => {
      errAMQC = false;
      amqcListener.subscribe(topic, (message, error) => {
        parseConveyor(message);
      });
    },
    (err) => {
      if (!errAMQC) {
        errAMQC = true;
        console.error('error connecting to AMQC');
      }
      setTimeout(listenScale, 5000);
    });
}

function listenPrintTopic(topic) {
  const amqcListener = new Stomp(serverConfig.PLCServer.host, serverConfig.PLCServer.port, serverConfig.PLCServer.username, serverConfig.PLCServer.password);
  amqcListener.connect(() => {
      errAMQC = false;
      amqcListener.subscribe(topic, (message, error) => {
        console.log('listenPrintTopic');
        console.log(message);
      });
    },
    (err) => {
      if (!errAMQC) {
        errAMQC = true;
        console.error('error connecting to AMQC');
      }
      setTimeout(listenScale, 5000);
    });
}

function listenBHSStatus() {
  const amqcListener = new Stomp(serverConfig.PLCServer.host, serverConfig.PLCServer.port, serverConfig.PLCServer.username, serverConfig.PLCServer.password);
  amqcListener.connect(() => {
      errAMQC = false;
      amqcListener.subscribe(topic, (message, error) => {
        parseBHSStatus(message);
      });
    },
    (err) => {
      if (!errAMQC) {
        errAMQC = true;
        console.error('error connecting to AMQC');
      }
      setTimeout(listenBHSStatus, 5000);
    });
}

function listenPLCTags() {
  const amqcListener = new Stomp(serverConfig.PLCServer.host, serverConfig.PLCServer.port, serverConfig.PLCServer.username, serverConfig.PLCServer.password);
  const topic = serverConfig.PLCTagsIn;
  amqcListener.connect(() => {
      errAMQC = false;
      amqcListener.subscribe(topic, (message, error) => {
        parseOPCJMS(message);
      });
    },
    (err) => {
      if (!errAMQC) {
        errAMQC = true;
        console.error('error connecting to AMQC');
      }
    });
}

function parseConveyor(message) {
  let origin = message.split('/')[0];
  let kioskName = message.split('/')[1];
  let command = message.split('/')[2];
  let tempObj = getOrCreateKiosk(kioskName);
  tempObj.ConveyorInject = 1;
}

function getOrCreateKiosk(kioskName) {
  let kiosk;
  kiosk = _.find(kioskInfo.kiosks, function (obj) {
    return obj.kioskId === kioskName;
  });
  if (!kiosk) {
    addKiosk(kioskName);
    kiosk = _.find(kioskInfo.kiosks, function (obj) {
      return obj.kioskId === kioskName;
    });
  }
  return kiosk;
}

function addKiosk(kioskName) {
  kioskInfo.kiosks.push({
    kioskId: kioskName,
    alias: 'Check-in Desk' + kioskName,
    nbrConveyors: 2,
    scaleConveyor: 1,
    minWeight: 5.0,
    maxWeight: 32.0,
    forwardAndReverseSupported: true,
    ConveyorForward: 0,
    ConveyorInject: 0,
    ConveyorReverse: 0,
    Error: 0,
    Info: 0,
    CurrentWeight: 0.0,
    Status: 0
  });
}

/*
  {
    "udropid": "IER_SBD_001",
    "conveyor1": "",
    "conveyor2": "",
    "bagpresent": 0,
    "bagprocessing": 0,
    "overheight": 0,
    "overlength": 0,
    "overweight": 0,
    "open": false,
    "devices": {
      "cuss": null,
      "bagtagprinter": null,
      "receiptprinter": null,
      "plc": {
        "connected": false,
        "error": 0
      },
      "gun": {
        "connected": false,
        "error": 0
      },
      "scale": {
        "connected": false,
        "error": 0
      },
      "bhs": {
        "ready": false
      }
    }
  }
*/
function parseBHSStatus(statusObj) {
  let kiosk = getOrCreateKiosk(statusObj.udropid);
  kiosk.BagPresent = statusObj.bagpresent;
  kiosk.BagProcessing = statusObj.bagprocessing;
  kiosk.OverHeight = statusObj.overheight;
  kiosk.OverLength = statusObj.overlength;
  kiosk.OverWeight = statusObj.overweight;
  kiosk.Open = statusObj.open;
  kiosk.Error = statusObj.devices.plc.error;
  kiosk.ErrorLabel = getErrorMessage(statusObj.devices.plc.error);
  kiosk.Ready = statusObj.devices.bhs.ready;
  if (kiosk.Error)
    notifyError(kiosk.kioskId, kiosk.Error);
}

// Channel1.PLC.IER_SBD_001_ConveyorInject=1
function parseOPCJMS(message) {
  let tagValue = message.split('=')[1];
  let splitTag = message.split('=')[0];
  splitTag = splitTag.split('_');
  let command = splitTag.pop();
  splitTag = splitTag.join('_').split('.');
  let kioskId = splitTag.pop();

  if (command == 'ConveyorInject' ||
    command == 'ConveyorForward' ||
    command == 'ConveyorReverse') {
    console.log('inside OPCJMS with command: ', message)

    console.log('tagValue', tagValue);
    console.log('command', command);
    console.log('kioskId', kioskId);
    let tempObj = getOrCreateKiosk(kioskId);
    switch (command) {
      case 'ConveyorInject':
        tempObj.ConveyorInject = tagValue;
        break;
      case 'ConveyorForward':
        tempObj.ConveyorForward = tagValue;
        break;
      case 'ConveyorReverse':
        tempObj.ConveyorReverse = tagValue;
        break;
    }
  } else if (command == 'Error' ||
    command == 'Status' ||
    command == 'Info') {
    console.log('inside OPCJMS with Tag: ', message)
    let tempObj = getOrCreateKiosk(kioskId);
    switch (command) {
      case 'Error':
        tempObj.Error = tagValue;
        tempObj.ErrorMessage = getErrorMessage(tagValue);
        break;
      case 'Status':
        tempObj.Status = tagValue;
        break;
      case 'Info':
        tempObj.Info = tagValue;
        break;
    }
  } else if (command == 'BHSLifeSign') {
    // console.log('inside OPCJMS with LIFESIGN: ', message)
  }
}

// INHERITED FROM APSB SBD SERVICE
function nextPubSubID() {
  var ticks = ((new Date().getTime()) + 62135596800000)
  var increment = 1000;
  var new_id = '' + ticks + increment;
  while (new_id < pubsubID) {
    increment += 10;
    new_id = '' + ticks + increment;
  }
  pubsubID = new_id;
  return new_id;
};

start();
listenScale();
listenPLCTags();
// listenPrintTopic('/topic/SIMULATOR-HARDWAREHUB-EVENTS');