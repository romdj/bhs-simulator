// var require: NodeRequire
// (id: string) => any
const log4js = require('log4js');
const fs = require('fs');
const express = require('express');
const Stomp = require('stomp-client');
const Moment = require('moment');

const UDropServerName = 'toto';
var cpt = 0;
var scaleWeight = 0.0;
var kiosks = {};
var theRestServer;
let errAMQC = false;
configureLogJs();

var theMainLogger = log4js.getLogger("Main");

// Decode command line arguments

process.argv.forEach((val, index) => {
  var name_and_val = val.split('=');
  if (name_and_val[0].toLowerCase() == "-config" && name_and_val.length > 1) {
    theConfigFileName = name_and_val[1];
  }
});

const tmp = fs.readFileSync(theConfigFileName, 'utf8');
const theParameters = JSON.parse(tmp);
// Read configuration from config file
theMainLogger.info("========================= PROCESS STARTED ===========================");
theMainLogger.info("Reading parameters in " + theConfigFileName);


startRestServer();

const amqcEmitter = new Stomp(theParameters.PLCServer.host, theParameters.PLCServer.port, theParameters.PLCServer.username, theParameters.PLCServer.password);

amqcEmitter.connect((sessionId) => {
  console.error('Successfully connected to amqc with ID:', sessionId);
  errAMQC = false;
}, (err) => {
  if (!errAMQC) {
    errAMQC = true;
    console.error('error connecting to amqc');
  }
});
scaleFeeder();

function configureLogJs() {
  var tmp = fs.readFileSync(__dirname + '/' + "log4js.json", 'utf8');

  var x1, x2 = 0;
  var var_name, regex;
  do {
    x1 = tmp.indexOf("${");
    if (x1 >= 0) {
      x2 = tmp.indexOf("}", x1);
      if (x2 > x1) {
        var_name = tmp.substring(x1 + 2, x2);
        regex = RegExp("\\$\\{" + var_name + "\\}", 'g');
        tmp = tmp.replace(regex, process.env[var_name]);
      }
    }
  } while (x1 >= 0 && x2 > x1);
  log4js.configure(JSON.parse(tmp));
}

function startRestServer() {
  theRestLogger = log4js.getLogger("REST");
  theRestApplication = express();
  theRestApplication.use(express.json());

  theRestApplication.get("/RestIoServerUrl", restHandleRestIoServerUrl);
  theRestApplication.get("/BagList", restHandleBagList);
  theRestApplication.get("/GetKioskContent/:kiosk", getKioskContent);
  theRestApplication.get("/getConveyorCommand/:kiosk", getConveyorCommand);
  theRestApplication.post("/addConveyorBag/:kiosk/:conveyor", addConveyorBag);
  theRestApplication.get("/removeConveyorBag/:kiosk/:conveyor", removeConveyorBag);
  theRestApplication.get("/reset/:kiosk", resetKioskData);
  theRestApplication.get("/Injected", restHandleInjected);
  theRestApplication.get("/updateWeight/:kiosk/:scaleWeight", updateScaleWeight);
  theRestApplication.get("/registerKiosk/:kiosk", registerKiosk);
  theRestApplication.get("/commandFinished/:command/:kiosk", commandFinished);
  theRestApplication.get("/simulateError/:error/:kiosk", simulateError);
  theRestApplication.get("/simulateStatus/:status/:kiosk", simulateStatus);
  theRestApplication.get("/simulateInfo/:info/:kiosk", simulateInfo);
  theRestApplication.get("/sendScale/:sending/:kiosk", sendScale);
  theRestApplication.post("/sendSBDStatus/:kiosk", sendSBDStatus);

  theRestApplication.post("/KioskObject/:kiosk", replaceKioskContent);

  theRestApplication.use(express.static("WebInterface"));

  theRestServer = theRestApplication.listen(theParameters.rest.serverPort, function () {
    var host = theRestServer.address().address;
    var port = theRestServer.address().port;

    theRestLogger.info('Web server Listening at http://' + host + ":" + port);
  });
}

function restHandleRestIoServerUrl(aRequest, aResponse) {
  aResponse.status(200).send(theParameters.RestIoServer.url);
}

function restHandleBagList(aRequest, aResponse) {
  aResponse.status(200).send(theParameters.bags);
}

function restHandleInjected(aRequest, aResponse) {
  aResponse.status(200).send(theParameters.bags);
}

function updateScaleWeight(aRequest, aResponse) {
  if (!kiosks[aRequest.params.kiosk])
    kiosks[aRequest.params.kiosk] = {
      weight: aRequest.params.scaleWeight
    };
  else
    kiosks[aRequest.params.kiosk].weight = aRequest.params.scaleWeight;
  // kiosks[aRequest.params.kiosk] = aRequest.params.scaleWeight;
  aResponse.status(200).send('scaleWeight for kiosk ' + aRequest.params.kiosk + ' updated with weight' + aRequest.params.scaleWeight);
}

function registerKiosk(aRequest, aResponse) {
  if (!kiosks[aRequest.params.kiosk]) {
    kiosks[aRequest.params.kiosk] = {
      weight: 0.0,
      conveyors: [{
          bag: null
        },
        {
          bag: null
        }
      ]
    };
    if (aRequest.params.threeConveyors) kiosks.conveyors.push({
      bag: null
    });
    console.log('new Kiosk(', aRequest.params.kiosk, ') created');
  }
  aResponse.status(200).send('new kiosk ' + aRequest.params.kiosk + ' registered');
}

function getKioskContent(aRequest, aResponse) {
  aResponse.status(200).send(kiosks[aRequest.params.kiosk]);
}

function addConveyorBag(aRequest, aResponse) {
  let kiosk = aRequest.params.kiosk;
  let conveyor = aRequest.params.conveyor;
  kiosks[kiosk].conveyors[conveyor].bag = aRequest.body;
}

function removeConveyorBag(aRequest, aResponse) {
  let conveyor = aRequest.params.conveyor;
  let kiosk = aRequest.params.kiosk;
  kiosks[kiosk].conveyors[conveyor].bag = null;
}

function commandFinished(aRequest, aResponse) {
  const command = aRequest.params.command;
  const kiosk = aRequest.params.kiosk;
  const message = 'Channel1.PLC.' + kiosk + '_Conveyor' + command + '=0';
  console.log('Emitting Command ', message);
  amqcEmitter.publish(theParameters.hardwarehubTopic, message);
  aResponse.status(200).send();
}

function simulateError(aRequest, aResponse) {
  const error = aRequest.params.error;
  const kiosk = aRequest.params.kiosk;
  const message = 'Channel1.PLC.' + kiosk + '_Error=' + error;
  console.log('Emitting error ', message);
  amqcEmitter.publish(theParameters.hardwarehubTopic, message);
  aResponse.status(200).send();
}

function simulateStatus(aRequest, aResponse) {
  const status = aRequest.params.status;
  const kiosk = aRequest.params.kiosk;
  const message = 'Channel1.PLC.' + kiosk + '_Status=' + status;
  console.log('Received ', aRequest.params);
  console.log('Emitting status ', message);
  amqcEmitter.publish(theParameters.hardwarehubTopic, message);
  aResponse.status(200).send();
}

function simulateInfo(aRequest, aResponse) {
  const info = aRequest.params.info;
  const kiosk = aRequest.params.kiosk;
  const message = 'Channel1.PLC.' + kiosk + '_Info=' + info;
  console.log('Emitting info ', message);
  amqcEmitter.publish(theParameters.hardwarehubTopic, message);
  aResponse.status(200).send();
}

function resetKioskData() {
  kiosks[aRequest.params.kiosk].weight = 0.0;
  kiosks[aRequest.params.kiosk].conveyors.forEach((conv) => {
    conv.bag = null;
  });
}

function replaceKioskContent(aRequest, aResponse) {
  let kiosk = aRequest.params.kiosk;
  kiosks[kiosk].conveyors[conveyor] = aRequest.body;
}

function getConveyorCommand(aRequest, aResponse) {
  let kiosk = aRequest.params.kiosk;
  let command = aRequest.params.command;
  amqcEmitter.publish(theParameters.conveyorTopic, command);

}

function sendScale(aRequest, aResponse) {
  const sending = aRequest.params.sending == '1' || aRequest.params.sending == 'true';
  console.log('toggling Scale with value', sending);
  const kiosk = aRequest.params.kiosk;
  kiosks[kiosk].sendScale = sending;
  aResponse.status(200).send();
}

function scaleFeeder() {
  for (key in kiosks) {
    // if (kiosks[key].sendScale) {
    let headers = {
      sourceclient: 'IER_SBD_001',
      Topic: key,
      ClientName: 'Devices Server',
      PubSubID: 'BHS Simulator ' + key,
      persistent: 'true',
      subscription: 'sub-0',
      expires: '0',
      timestamp: Moment().valueOf(),
      sourcedevice: 'Scale',
      MachineName: UDropServerName
    };
    amqcEmitter.publish(theParameters.scaleTopic, formatWeightMsg(key, kiosks[key]), headers);
    // }
    // console.log('sending message ', formatWeightMsg(key, kiosks[key]), 'with key',key, 'with headers',headers);
  }
  // amqcEmitter.disconnect(() => {})
  setTimeout(scaleFeeder, theParameters.updateRate);
}

function formatWeightMsg(kiosk, kioskObj) {
  if (typeof kioskObj.weight == 'number') {
    kioskObj.weight = (kioskObj.weight).toFixed(5)
  } else if (typeof kioskObj.weight == 'string') {
    if (kioskObj.weight.split(',').length < 2)
      kioskObj.weight = kioskObj.weight + ',00';
  }
  return 'B=  ' + kioskObj.weight;
}

function formatConveyorCommandMsg(kiosk, kioskObj) {
  return ['UDROP', kiosk, kioskObj.weight, 'STATUS/'].join('/');
}

function sendSBDStatus(aRequest, aResponse) {
  const SBDData = aRequest.body.sbdStatus;
  const kiosk = aRequest.params.kiosk;
  const headers = {
    'content-length': SBDData.length,
    Origin: 'AMQClient',
    ID: 9999
  }
  amqcEmitter.publish(theParameters.SBDStatusTopic, JSON.stringify(SBDData), headers);
  aResponse.status(200).send();
}