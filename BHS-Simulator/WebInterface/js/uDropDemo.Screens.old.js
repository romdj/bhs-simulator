// Main controller
screensApp.controller('uDropDemoSimulatorController', function ($scope, $rootScope, $http, $interval, $timeout, poller, Screen, ScreenEvents, ScreenSaver, ScreenNotify) {

  $scope.Screen = Screen;

  // Debug mode
  $scope.debugMode = false;

  $scope.onDragComplete = function (data, evt) {
    console.log("drag success, data:", data);
    data.OnConveyor = true;
  }

  $scope.onBagDroppedOnConveyor = function (data, evt) {
    console.log("Bag " + data.LPC + " dropped on conveyor.");
    $scope.Screen.data.view.SimulatorData.BagPresent = true;
    $scope.Screen.data.view.SimulatorData.Bags.push(data);
    $scope.Screen.data.view.SimulatorData.OverLength = data.OverLength;
    $scope.Screen.data.view.SimulatorData.OverHeight = data.OverHeight;
    data.OnConveyor = true;
    data.PositionOnBelt = 0;
  }

  $scope.onBagRemovedFromConveyor = function (data, evt) {
    console.log("Bag " + data.LPC + " removed from conveyor.");

    data.OnConveyor = false;
    var index = $scope.Screen.data.view.SimulatorData.Bags.indexOf(data);

    if (index >= 0)
      $scope.Screen.data.view.SimulatorData.Bags.splice(index, 1);

    $scope.Screen.data.view.SimulatorData.BagPresent = $scope.Screen.data.view.SimulatorData.Bags.length > 0;

    $scope.Screen.data.view.SimulatorData.OverLength = false;
    $scope.Screen.data.view.SimulatorData.OverHeight = false;
  }

  $scope.EStopToggled = function () {
    console.log("E-Stop toggled : new value : " + !$scope.Screen.data.view.SimulatorData.EmergencyStop);
    $scope.Screen.data.view.SimulatorData.EmergencyStop = !$scope.Screen.data.view.SimulatorData.EmergencyStop;

    $scope.Screen.data.view.SimulatorData.ConveyorMoving &= !$scope.Screen.data.view.SimulatorData.EmergencyStop;
  }

  function publishMessage(topic, messageText) {
    var url_base = "/CommonServices/";
    var msg = {
      topic: topic,
      message: messageText
    }
    console.log("Publishing on topic " + topic + " : " + messageText);
    $.ajax({
      type: "POST",
      url: url_base + "jms/Publish",
      data: JSON.stringify(msg),
      contentType: "application/json; charset=utf-8",
      dataType: "json",
    });
  }


  $scope.simulateBarcodeScanning = function (barcodeString) {
    var msg = {
      text: barcodeString,
      sourceclient: $scope.Screen.id,
      sourcedevice: "Gun"
    }

    publishMessage("UDROPDEMOSIMULATEDGUN", JSON.stringify(msg));
  }


  function generateWeightMessage() {
    var weight = 0;
    for (i = 0; i < $scope.Screen.data.view.SimulatorData.Bags.length; i++) {
      weight += $scope.Screen.data.view.SimulatorData.Bags[i].Weight;
    }
    console.log("total weight :" + weight);

    var msg = "WEIGHT/" + $scope.Screen.id + "/" + weight + "/OK/";
    return msg;
  }


  function generateStatusMessage() {
    var plc_error = 0;

    if ($scope.Screen.data.view.SimulatorData.EmergencyStop)
      plc_error = 1;

    var msg = {
      "udropid": $scope.Screen.id,
      "conveyor1": "",
      "conveyor2": $scope.Screen.data.view.SimulatorData.ConveyorMoving ? "Inject" : "",
      "bagpresent": $scope.Screen.data.view.SimulatorData.BagPresent ? 1 : 0,
      "bagprocessing": 0,
      "overheight": $scope.Screen.data.view.SimulatorData.OverHeight ? 1 : 0,
      "overlength": $scope.Screen.data.view.SimulatorData.OverLength ? 1 : 0,
      "overweight": 0,
      "open": $scope.Screen.data.view.SimulatorData.PositionOpen,
      "devices": {
        "cuss": null,
        "bagtagprinter": null,
        "receiptprinter": null,
        "plc": {
          "connected": true,
          "error": plc_error
        },
        "gun": {
          "connected": true,
          "error": 0
        },
        "scale": {
          "connected": true,
          "error": 0
        },
        "bhs": {
          "ready": true
        }
      }
    }
    var msg_str = JSON.stringify(msg);
    //console.log("status msg : "+msg_str);
    return msg_str;
  }

  function injectBag() {
    console.log("Starting conveyor");
    $scope.Screen.data.view.SimulatorData.ConveyorMoving = true;

    var pos = 0;
    var max_pos = 2000;
    var increment = 40; // Change here if you want to modify the speed of the bag on the conveyor
    var conveyor_entrance = $("#ConveyorEntrance");
    if (conveyor_entrance) {
      pos = conveyor_entrance.offset().left + 50;
    } else {
      console.log("Cannot find #ConveyorEntrance");
    }

    var conveyor_exit = $("#ConveyorExit");
    if (conveyor_exit) {
      max_pos = conveyor_exit.offset().left + 50;
    } else {
      console.log("Cannot find #ConveyorExit");
    }
    console.log("Pos=" + pos + " MaxPos=" + max_pos);
    var inject_worker = function () {
      if ($scope.Screen.data.view.SimulatorData.EmergencyStop)
        return;
      if ($scope.Screen.data.view.SimulatorData.OverLength)
        return;
      if ($scope.Screen.data.view.SimulatorData.OverHeight)
        return;
      pos += increment;
      console.log("pos:" + pos);
      var still_a_bag = false;

      still_a_bag = false;
      for (i = 0; i < $scope.Screen.data.view.SimulatorData.Bags.length; i++) {
        var id = "#ConveyorBag" + i;
        var cur_pos = $(id).offset().left;
        console.log("Current position of " + id + " is " + cur_pos);

        $(id).offset({
          left: cur_pos + increment
        });

        var new_pos = $(id).offset().left;
        if (new_pos > max_pos) {
          $(id).hide();
          $scope.Screen.data.view.SimulatorData.Bags.splice(i);
          console.log(id + " has reached the end of the conveyor")
        } else {
          still_a_bag = true;
          console.log("New position of " + id + " is now " + new_pos);
        }
      }
      if (still_a_bag || pos < max_pos) {
        $timeout(inject_worker, 100); // reset the timer
      } else {
        $scope.Screen.data.view.SimulatorData.ConveyorMoving = false;
        $scope.Screen.data.view.SimulatorData.BagPresent = false;
        //$scope.Screen.data.view.SimulatorData.Bags[i].PositionOnBelt=-1;
      }
    }
    $timeout(inject_worker, 100); // start the timer
  }

  function init() {
    console.log("uDropDemoSimulatorController initialization");

    $scope.statusInterval = 1000; //ms
    var status_timer_worker = function () {
      publishMessage("SBDSTATUS", generateStatusMessage());
      $timeout(status_timer_worker, $scope.statusInterval); // reset the timer
    }
    // Start the timer
    $timeout(status_timer_worker, $scope.statusInterval);

    $scope.weightInterval = 1000; //ms
    var weight_timer_worker = function () {
      console.log("Publishing WEBMESSAGE with weight...");
      publishMessage("WEBMESSAGE", generateWeightMessage());
      $timeout(weight_timer_worker, $scope.weightInterval); // reset the timer
    }
    // Start the timer
    $timeout(weight_timer_worker, $scope.weightInterval);


    // Registering for events
    ScreenEvents.register("INJECT", function (event) {
      console.log("InjectBag command received");
      console.log(event);
      injectBag();
    });
  }
  init();
});