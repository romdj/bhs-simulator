var myAppModule = angular.module('BHSSimulatorApp', ["ui.bootstrap", "ngDraggable", "ngAnimate"]);
var jq = $.noConflict();

myAppModule.controller('BHSSimulatorController', function ($scope, $http, $location, $timeout, $interval) {


  $scope.simulateSBD = false;
  $scope.SBDData = {
    udropid: $scope.kioskId,
    conveyor1: '',
    conveyor2: '',
    bagpresent: 0,
    bagprocessing: 0,
    overheight: 0,
    overlength: 0,
    overweight: 0,
    open: true,
    devices: {
      cuss: null,
      bagtagprinter: {
        paperout: false,
        papernearend: false,
        connected: false
      },
      receiptprinter: {
        paperout: false,
        papernearend: false,
        connected: false
      },
      plc: {
        connected: false,
        error: 0
      },
      gun: {
        connected: false,
        error: 0
      },
      scale: {
        connected: false,
        error: 0
      },
      bhs: {
        ready: false
      }
    }
  };
  // $scope.SBDData.devices.bagtagprinter.connected;
  // $scope.SBDData.devices.receiptprinter.connected;
  // $scope.SBDData.devices.plc.connected;
  // $scope.SBDData.devices.gun.connected;
  // $scope.SBDData.devices.scale.connected;
  // $scope.SBDData.devices.bhs.ready;
  $scope.angle = 0;
  $scope.scaleThreeConveyorPosition = 0;
  $scope.sendScaleMessage = false;
  $scope.errorCode = '';
  var theRestIoServerUrl = "";
  $scope.threeConveyors, $scope.debounceScale = false;
  $scope.kiosk = {
    ConveyorInject: 0,
    Error: 0,
    ErrorMessage: '',
    Info: 0,
    WeightLimit: 0,
    CurrentWeight: 0,
    Status: 0
  }

  $scope.supportedBagColours = ['black', 'blue', 'brown', 'cyan', 'gray', 'orange', 'purple', 'red', 'green', 'yellow'];
  $scope.bagColour = '';
  $scope.kiosk.ConveyorForward = ($scope.threeConveyors) ? 0 : null;
  $scope.kiosk.ConveyorReverse = ($scope.threeConveyors) ? 0 : null;
  $scope.travelerBagFiddle = false;
  $scope.restApiServer = null;
  $scope.SimulatorData = {
    Info: 0,
    Status: 0,
    Error: 0,
    OverHeight: false,
    OverLength: false,
    OverWeight: false,
    UnderWeight: false,
    ScaleValue: 0.0,
    minWeight: 5,
    maxWeight: 45,
    BagReserve: [],
    InfoMessage: '',
    ErrorMessage: '',
    ScalePosition: (!$scope.threeConveyors) ? 0 : $scope.scaleThreeConveyorPosition,
    Conveyors: [{
        BagPresent: false,
        Moving: false,
        CanInject: true,
        hasScale: !$scope.threeConveyors,
        previousConveyor: null,
        nextConveyor: 1,
        CanForward: $scope.threeConveyors,
        Bags: [],
        colour: 'grey',
        cssClass: 'conv1',
        label: 'Conveyor 1'
      }, {
        BagPresent: false,
        Moving: false,
        CanInject: true,
        hasScale: $scope.threeConveyors,
        previousConveyor: 0,
        nextConveyor: 2,
        CanForward: $scope.threeConveyors,
        Bags: [],
        colour: 'grey',
        cssClass: 'conv2',
        label: 'Conveyor 2'
      },
      {
        BagPresent: false,
        Moving: false,
        CanInject: false,
        previousConveyor: 1,
        nextConveyor: null,
        CanForward: false,
        Bags: [],
        colour: 'grey',
        cssClass: 'conv3',
        label: 'Conveyor 3'
      }
    ],
    Collector: {
      Bags: [],
      isReady: false,
      Moving: false,
      colour: 'darkgrey;',
      injectionDelay: Math.random() * 5000 + 2000
    }
  }

  function computeBHSStates() {
    $scope.SimulatorData.OverLength = ($scope.SimulatorData.Conveyors[0].BagPresent && $scope.SimulatorData.Conveyors[1].BagPresent);
    $scope.SimulatorData.OverWeight = ($scope.SimulatorData.CurrentWeight < $scope.SimulatorData.minWeight && $scope.SimulatorData.CurrentWeight > $scope.SimulatorData.maxWeight);
  }

  $scope.reset = function () {
    for (let conv of $scope.SimulatorData.Conveyors) {
      conv.colour = 'grey;';
      conv.Bags = [];
      conv.BagPresent = false;
    }
    $scope.SimulatorData.Collector.Bags = [];
    $scope.SimulatorData.InfoMessage = '';
    $scope.SimulatorData.ErrorMessage = '';
  }

  $scope.transit = function (arraySrcDest) {
    if (arraySrcDest.length > 0 && $scope.kiosk.Error == 0) {
      let obj = arraySrcDest[0];
      let returnArray = arraySrcDest.slice(1, arraySrcDest.length);
      let delay = (obj.isCollector) ? $scope.SimulatorData.Collector.injectionDelay : 1500;
      if (obj.isCollector) {
        console.log('obj.source', obj.source)
        $scope.SimulatorData.Conveyors[obj.source].Moving = true;
        // $scope.SimulatorData.Collector.Moving = true;
        $scope.SimulatorData.Collector.Bags.push($scope.SimulatorData.Conveyors[obj.source].Bags.pop());
        $timeout(() => {
          $scope.SimulatorData.Collector.BagPresent = true;
          $scope.refreshKioskData();
          $scope.SimulatorData.Conveyors[obj.source].BagPresent = false;
          $scope.SimulatorData.Collector.BagPresent = false;
          $scope.SimulatorData.Conveyors[obj.source].Moving = false;
          $scope.SimulatorData.Collector.Moving = false;
          $scope.SimulatorData.Collector.Bags.pop();
          $scope.SimulatorData.Collector.BagPresent = $scope.SimulatorData.Collector.Bags.length > 0;
          $scope.refreshKioskData();
        }, delay);
      } else if (obj.source > obj.dest) {
        $scope.SimulatorData.Conveyors[obj.source].MovingReverse = true;
        $scope.SimulatorData.Conveyors[obj.dest].MovingReverse = true;
        $scope.SimulatorData.Conveyors[obj.dest].Bags.push($scope.SimulatorData.Conveyors[obj.source].Bags.pop());
        $timeout(() => {
          $scope.SimulatorData.Conveyors[obj.dest].BagPresent = true;
          $scope.refreshKioskData();
          $scope.SimulatorData.Conveyors[obj.source].BagPresent = false;
          $scope.SimulatorData.Conveyors[obj.dest].BagPresent = false;
          $scope.SimulatorData.Conveyors[obj.source].MovingReverse = false;
          $scope.SimulatorData.Conveyors[obj.dest].MovingReverse = false;
          $scope.transit(returnArray);
        }, delay);
      } else {
        $scope.SimulatorData.Conveyors[obj.source].Moving = true;
        $scope.SimulatorData.Conveyors[obj.dest].Moving = true;
        $scope.SimulatorData.Conveyors[obj.dest].Bags.push($scope.SimulatorData.Conveyors[obj.source].Bags.pop());
        $timeout(() => {
          $scope.SimulatorData.Conveyors[obj.dest].BagPresent = true;
          $scope.refreshKioskData();
          $scope.SimulatorData.Conveyors[obj.source].BagPresent = false;
          $scope.SimulatorData.Conveyors[obj.dest].BagPresent = false;
          $scope.SimulatorData.Conveyors[obj.source].Moving = false;
          $scope.SimulatorData.Conveyors[obj.dest].Moving = false;
          $scope.transit(returnArray);
        }, delay);
      }
    }
  }

  $scope.inject = async function () {
    let canInject = $scope.canInject();
    if (canInject.result) {
      let transition = ($scope.threeConveyors) ? [{
          source: 1,
          dest: 2,
          isCollector: false
        },
        {
          source: 2,
          isCollector: true
        }
      ] : [{
          source: 0,
          dest: 1,
          isCollector: false
        },
        {
          source: 1,
          isCollector: true
        }
      ]
      $scope.transit(transition);
      $scope.endCommand('Inject');

    } else {
      $scope.SimulatorData.ErrorMessage = 'cannot Inject!' + canInject.reason;
    }
  }

  $scope.forward = function () {
    let canForward = $scope.canForward();
    if (canForward.result) {
      let transition = [{
        source: 0,
        dest: 1,
        isCollector: false
      }];
      $scope.transit(transition);
      $scope.endCommand('Forward');
    } else {
      $scope.SimulatorData.ErrorMessage = 'cannot Forward!' + canForward.reason;
    }
  }

  $scope.reverse = function () {
    let canReverse = $scope.canReverse();
    if (canReverse.result) {
      let transition = [{
        source: 1,
        dest: 0,
        isCollector: false
      }];
      $scope.transit(transition);
      $scope.endCommand('Reverse');
    } else {
      $scope.SimulatorData.ErrorMessage = 'cannot Reverse!' + canReverse.reason;
    }

  }

  $scope.endCommand = function (command) {
    console.log('inside endCommand');
    $http.get(["/commandFinished", command, $scope.kioskId].join('/'))
      .success(function (response) {
        console.log('Success finishing request for command ', command, ' and kiosk: ', $scope.kioskId);
      })
      .error(function (error) {
        console.log('error finishing request for command ', command, ' and kiosk: ', $scope.kioskId, ' error: ', error);
      });
  }

  $scope.bhsHasBag = function () {
    for (conv of $scope.SimulatorData.Conveyors) {
      if (conv.Bags.length > 0)
        return true;
    }
    return false;
  }

  $scope.conveyorHasBag = function (conveyorNb) {
    return ($scope.SimulatorData.Conveyors[conveyorNb].Bags.length > 0);
  }

  $scope.removeBag = function (conveyorNb) {
    if ($scope.SimulatorData.Conveyors[conveyorNb].Moving || $scope.SimulatorData.Conveyors[conveyorNb].MovingReverse) {
      $scope.simulateError(94);
      $scope.SimulatorData.Conveyors[conveyorNb].Moving = false;
      $scope.SimulatorData.Conveyors[conveyorNb].MovingReverse = false;
      $scope.SimulatorData.Conveyors[conveyorNb].Bags.pop();
    }
    $scope.SimulatorData.Conveyors[conveyorNb].Bags = [];

    $scope.refreshKioskData();
  }

  $scope.bhsOK = function () {
    let returnValue = {
      result: $scope.SimulatorData.Error == 0,
      reason: $scope.errorList[$scope.SimulatorData.Error]
    };
    if (!returnValue.result) {
      return returnValue;
    } else {
      if ($scope.OverHeight) {
        returnValue.result = false;
        returnValue.reason = 'OverHeight';
        return returnValue;
      }
      if ($scope.OverLength) {
        returnValue.result = false;
        returnValue.reason = 'OverLength';
        return returnValue;
      }
    }
    return returnValue;
  }

  /**
   * The CanInject Function verifies the following:
   * @test: bag Present on appropriate conveyor
   * @test: no bag jam on injection conveyor
   * @test: if BHS Error -> return bhs error 
   */
  $scope.canInject = function () {
    if ($scope.bhsOK().result) {
      let returnValue = {
        result: true,
        reason: 'All ok, Bag can be injected'
      };
      let conveyors = $scope.SimulatorData.Conveyors;
      if ($scope.threeConveyors) {
        if (conveyors[1].Bags.length != 1) {
          returnValue.result = false;
          returnValue.reason = 'no bag on Expected conveyor (1)';
        }
      } else {
        if (conveyors[0].Bags.length != 1)
          returnValue.result = false;
        returnValue.reason = 'no bag on Expected conveyor (0)';
      }
      return returnValue;
      // if($scope.bhsHasBag() 
      //   && conveyors)
    } else return $scope.bhsOK();
  }

  /**
   * The CanForward Function verifies the following:
   * @test: bag Present on appropriate conveyor
   * @test: no bag on destination conveyor
   * @test: no bag jam on forward conveyor
   * @test: if BHS Error -> return bhs error 
   */
  $scope.canForward = function () {
    if ($scope.bhsOK().result) {
      let returnValue = {
        result: ($scope.SimulatorData.Conveyors[0].Bags.length == 1 &&
          $scope.SimulatorData.Conveyors[1].Bags.length == 0 &&
          $scope.SimulatorData.Conveyors.length > 2 &&
          $scope.threeConveyors),
        reason: ''
      };
      if (returnValue.result) {
        returnValue.reason = 'All OK';
      } else {
        let msg;
        msg += ($scope.SimulatorData.Conveyors[0].Bags.length == 1) ? '' : 'No Bag on Conveyor 0';
        msg += ($scope.SimulatorData.Conveyors[1].Bags.length == 0) ? '' : 'Bag Present on Conveyor 1';
        msg += ($scope.SimulatorData.Conveyors.length > 2) ? '' : 'Not the right number of Conveyors(' + $scope.SimulatorData.Conveyors.length + ')';
        msg += ($scope.threeConveyors) ? '' : 'Not the right number of Conveyors (!threeConveyors)';
        returnValue.reason = msg;
      }
      return returnValue;
    } else return $scope.bhsOK();
  }

  $scope.canReverse = function () {
    if ($scope.bhsOK().result) {
      let returnValue = {
        result: ($scope.SimulatorData.Conveyors[0].Bags.length == 0 &&
          $scope.SimulatorData.Conveyors[1].Bags.length == 1 &&
          $scope.SimulatorData.Conveyors.length > 2 &&
          $scope.threeConveyors),
        reason: ''
      };
      if (returnValue.result) {
        returnValue.reason = 'All OK';
      } else {
        let msg;
        msg += ($scope.SimulatorData.Conveyors[0].Bags.length == 0) ? '' : 'Bag Present on Conveyor 0';
        msg += ($scope.SimulatorData.Conveyors[1].Bags.length == 1) ? '' : 'No Bag on Conveyor 1';
        msg += ($scope.SimulatorData.Conveyors.length > 2) ? '' : 'Not the right number of Conveyors(' + $scope.SimulatorData.Conveyors.length + ')';
        msg += ($scope.threeConveyors) ? '' : 'Not the right number of Conveyors (!threeConveyors)';
        returnValue.reason = msg;
      }
      return returnValue;
    } else return $scope.bhsOK();
  }

  $scope.colourSupported = function (colour) {
    for (elt of $scope.supportedBagColours) {
      if (elt == colour)
        return true;
    }
    $scope.SimulatorData.ErrorMessage = 'Colour ' + colour + ' entered not supported';
    return false;
  }

  $scope.buildBagObject = function (weight, colour) {
    return {
      Weight: weight,
      Image: './img/simulator/bag-' + colour + '.png'
    };
  }

  $scope.addBag = function (weight, colour, obj) {
    console.log(weight, colour, obj);
    for (key in obj)
      if (obj[key] === undefined) obj[key] = false;
    if ($scope.SimulatorData.Conveyors[0].Bags.length == 0) {
      if (weight) {
        if (!colour) colour = 'black';
        colour = colour.toLowerCase();
        if ($scope.colourSupported(colour)) {
          $scope.SimulatorData.Conveyors[0].Bags.push(Object.assign($scope.buildBagObject(weight, colour), obj));
          $scope.SimulatorData.InfoMessage = 'successfully added bag to conveyor 0';
        }
      }
    } else {
      $scope.SimulatorData.ErrorMessage = 'Cannot add new bag, Conveyor 0 Not empty';
    }
    $scope.refreshKioskData();
  }

  $scope.toggleThreeConveyors = function () {
    $scope.threeConveyors = !$scope.threeConveyors;
    if ($scope.threeConveyors) {
      $scope.SimulatorData.InfoMessage = 'Now simulating 3 Conveyors setup';
    } else {
      $scope.SimulatorData.InfoMessage = 'Now simulating 2 Conveyors setup';
    }
    $scope.adaptThreeConveyors();
  }

  $scope.adaptThreeConveyors = function () {
    // console.log('inside adaptThreeConveyors');
    for (scale of $scope.SimulatorData.Conveyors) scale.hasScale = false;
    if ($scope.threeConveyors) {
      $scope.SimulatorData.Conveyors[0].CanForward = true;
      $scope.SimulatorData.Conveyors[1].CanForward = true;
      $scope.SimulatorData.Conveyors[0].CanInject = false;
      $scope.SimulatorData.Conveyors[1].CanInject = true;
      $scope.SimulatorData.Conveyors[2].CanInject = true;
      $scope.SimulatorData.Conveyors[$scope.scaleThreeConveyorPosition].hasScale = true;
    } else {
      $scope.SimulatorData.Conveyors[0].CanForward = false;
      $scope.SimulatorData.Conveyors[1].CanForward = false;
      $scope.SimulatorData.Conveyors[0].CanInject = true;
      $scope.SimulatorData.Conveyors[0].hasScale = true;
    }
    $scope.refreshKioskData();
    setTimeout($scope.adaptThreeConveyors, 100);
  }

  $scope.refreshKioskData = function () {
    getErrorList();
    if ($scope.SimulatorData.Conveyors[0].Bags.length > 0) {
      $scope.SimulatorData.OverLength = $scope.SimulatorData.Conveyors[0].Bags[0].OverLength;
      $scope.SimulatorData.OverHeight = $scope.SimulatorData.Conveyors[0].Bags[0].OverHeight;

      // $scope.SBDData.bagpresent = $scope.SimulatorData.Conveyors[0].Bags[0].length > 0;
      $scope.SBDData.overlength = $scope.SimulatorData.Conveyors[0].Bags[0].OverLength;
      $scope.SBDData.overheight = $scope.SimulatorData.Conveyors[0].Bags[0].OverHeight;
      $scope.SBDData.overweight = $scope.SimulatorData.Conveyors[0].Bags[0].Weight > $scope.SimulatorData.maxWeight;
      
    } else {
      $scope.SimulatorData.OverLength = false;
      $scope.SimulatorData.OverHeight = false;
    }
    for (let conv of $scope.SimulatorData.Conveyors) {
      if (conv.hasScale) {
        if (conv.Bags.length > 0) {
          $scope.SimulatorData.ScaleValue = conv.Bags[0].Weight;
          $scope.SimulatorData.UnderWeight = conv.Bags[0].Weight < $scope.SimulatorData.minWeight;
          $scope.SimulatorData.OverWeight = conv.Bags[0].Weight > $scope.SimulatorData.maxWeight;
        } else {
          $scope.SimulatorData.ScaleValue = 0.0;
          $scope.SimulatorData.UnderWeight = false;
          $scope.SimulatorData.OverWeight = false;
        }
      }
    }
    updateKioskWeight();
    // (SimulatorData.Conveyors[SimulatorData.ScalePosition].Bags.length > 0)?SimulatorData.Conveyors[SimulatorData.ScalePosition].Bags[0].Weight:
  }

  function init() {
    console.log('url: ', $location.absUrl());
    var kioskId = $location.absUrl();
    kioskId = kioskId.split('?')[1].split('=')[1];
    $scope.kioskId = kioskId;
    $scope.SBDData.udropid = kioskId;
    $scope.threeConveyors = ($location.absUrl().split('?')[1].split('=')[2] == 'true');
    if (!kioskId) kioskId = 'IER_SBD_001';
    console.log('Fetching KioskID: ', kioskId);
    console.log("Initializing BHSSimulatorController...");
    // console.log("$scope.kiosk = " + JSON.stringify($scope.kiosk));
    getRestIoServerUrl(kioskId);
    registerKiosk(kioskId);
    // getInitKioskData(kioskId);
    // getBags();
    // injectBag();
    refreshKioskDataRoutine();
  }

  function getRestIoServerUrl(kioskId) {
    console.log("Getting url of REST IO server...")
    $http.get("RestIoServerUrl")
      .success(function (response) {
        theRestIoServerUrl = response + kioskId;
        console.log("REST IO Server url is " + theRestIoServerUrl);
        getErrorList();
      })
      .error(function (error) {
        console.log("error while getting RestIoServer url : " + error);
      });
  }

  function registerKiosk(kioskId) {
    $http.get("/registerKiosk/" + kioskId)
      .success(function (response) {
        console.log("Success saving kiosk ", kioskId);
      })
      .error(function (error) {
        console.log("error while saving kiosk : ", kioskId, ' error: ', error);
      });
  }

  function getErrorList() {
    let injectedURL = theRestIoServerUrl;
    injectedURL = injectedURL.split('/');
    while (injectedURL.length > 5)
      injectedURL.pop();
    injectedURL = injectedURL.join('/');
    injectedURL = injectedURL.replace('GETKIOSK', 'GETERRORLIST');
    $http.get(injectedURL)
      .success(function (response) {
        $scope.errorList = response;
      })
      .error(function (error) {
        console.log('Failure while fetching error messages list,  error: ', error);
      });
  }

  function getBags() {
    console.log("Getting bag list...")
    $http.get("BagList")
      .success(function (response) {
        treatBagListResponse(response);
      })
      .error(function (error) {
        console.log("error while getting bag list : " + error);
      });
  }

  function treatBagListResponse(aResponse) {
    $scope.Bags = aResponse;
    console.log("Received a list of " + $scope.Bags.length + " bags.");
    for (bag of $scope.Bags) {
      bag.Image = "img/simulator/" + bag.Image;
      bag.PopupVisible = false;
      bag.PositionOnBelt = 0;
      // console.log(bag.Image);
    }
  }

  function refreshKioskDataRoutine() {
    $http.get(theRestIoServerUrl)
      .success(function (response) {
        parseKioskData(response);
        let convInfo = $scope.computeStatus($scope.kiosk.Info);
        $scope.SimulatorData.Conveyors[0].BagPresent = convInfo.cellConveyor1;
        $scope.SimulatorData.Conveyors[1].BagPresent = convInfo.cellConveyor2;
        if ($scope.simulateSBD) {
          // $scope.SBDData.bagpresent = $scope.SimulatorData.Conveyors[0].BagPresent || $scope.SimulatorData.Conveyors[1].BagPresent;
          $scope.postSBDStatus();
        }
        setTimeout(refreshKioskDataRoutine, 1000);
      })
      .error(function (error) {
        console.log("error while getting kiosk data : " + error);
        setTimeout(refreshKioskDataRoutine, 1000)
      });
    $http.get(["/GetKioskContent", $scope.kioskId].join('/'))
      .success(function (response) {
        // console.log("GetKioskContent ", $scope.kioskId, "content: ", response);
      })
  }

  function parseKioskData(response) {
    if (response[0] != '<') {
      $scope.kiosk = response;
      for (key in $scope.kiosk) {
        $scope.SimulatorData[key] = $scope.kiosk[key];
      }
      if (Number($scope.kiosk.ConveyorInject) == 1) $scope.inject();
      if (Number($scope.kiosk.ConveyorForward) == 1) $scope.forward();
      if (Number($scope.kiosk.ConveyorReverse) == 1) $scope.reverse();
    }
  }

  // function updateKioskWeight(weight) {
  function updateKioskWeight() {
    let weight = $scope.SimulatorData.ScaleValue;
    weight += ($scope.travelerBagFiddle) ? Math.sin(($scope.angle++ * 30) / (2 * Math.PI)) : 0;
    weight = Math.round(weight * 10000) / 10000;
    $scope.SimulatorData.ScaleValue = weight;
    $http.get(["/updateWeight", $scope.kioskId, weight].join('/'))
      .success(function (response) {
        // console.log("Success saving weight for kiosk : ", $scope.kioskId, ' with weight: ', weight);
      })
      .error(function (error) {
        console.log("error while saving kiosk : ", $scope.kioskId, ' error: ', error);
      });
  }

  $scope.WriteStatus = function (msg) {
    $scope.SimulatorData.InfoMessage = msg;
  }

  $scope.key = function (aBoolean) {
    // let injectedURL = theRestIoServerUrl;
    // injectedURL = injectedURL.replace('BHSCMD', 'SIMULATOR').replace('GETKIOSK', 'KEY');
    // injectedURL += (aBoolean) ? '/1' : '/0';
    // $http.get(injectedURL);
    let status;
    let info;
    if (aBoolean) {
      status = 1;
      info = 32;
    } else {
      status = 0;
      info = 0;
    }
    $scope.simulateInfo(info);
    $scope.simulateStatus(status);
    let tempLogStr = (aBoolean) ? 'ON' : 'OFF';
    console.log('KEY ' + tempLogStr);
    $scope.SimulatorData.InfoMessage = 'KEY ' + tempLogStr;
  }

  $scope.es = function (aBoolean) {
    let injectedURL = theRestIoServerUrl;
    injectedURL = injectedURL.replace('BHSCMD', 'SIMULATOR').replace('GETKIOSK', 'FORCEERROR');
    injectedURL += (aBoolean) ? '/1' : '/0';
    $http.get(injectedURL);
    $scope.SimulatorData.InfoMessage = 'EMERGENCY STOP TOGGLE';
    if (aBoolean)
      $scope.SimulatorData.ErrorMessage = 'EMERGENCY STOP DETECTED';
  }

  $scope.pc = function (aBoolean) {
    let injectedURL = theRestIoServerUrl;
    injectedURL = injectedURL.replace('BHSCMD', 'SIMULATOR').replace('GETKIOSK', 'FORCEERROR');
    injectedURL += (aBoolean) ? '/2' : '/0';
    $http.get(injectedURL);
    $scope.SimulatorData.InfoMessage = 'POWER CUT TOGGLE';
    if (aBoolean)
      $scope.SimulatorData.ErrorMessage = 'POWER CUT DETECTED';
  }

  // Weight Issue
  $scope.wi = function (aBoolean) {
    let injectedURL = theRestIoServerUrl;
    injectedURL = injectedURL.replace('BHSCMD', 'SIMULATOR').replace('GETKIOSK', 'FORCEERROR');
    injectedURL += (aBoolean) ? '/3' : '/0';
    $http.get(injectedURL);
    $scope.SimulatorData.InfoMessage = 'WEIGHT ISSUE TOGGLE';
    if (aBoolean)
      $scope.SimulatorData.ErrorMessage = 'WEIGHT ISSUE DETECTED';
  }

  // HANDSAFE
  $scope.handSafe = function (aBoolean) {
    let injectedURL = theRestIoServerUrl;
    injectedURL = injectedURL.replace('BHSCMD', 'SIMULATOR').replace('GETKIOSK', 'FORCEERROR');
    injectedURL += (aBoolean) ? '/4' : '/0';
    $http.get(injectedURL);
    $scope.SimulatorData.InfoMessage = 'HANDSAFE TOGGLE';
    if (aBoolean)
      $scope.SimulatorData.ErrorMessage = 'HANDSAFE DETECTED';
  }

  // FEATURE NOT SUPPORTED
  $scope.noFeature = function (aBoolean) {
    let injectedURL = theRestIoServerUrl;
    injectedURL = injectedURL.replace('BHSCMD', 'SIMULATOR').replace('GETKIOSK', 'FORCEERROR');
    injectedURL += (aBoolean) ? '/5' : '/0';
    $http.get(injectedURL);
    $scope.SimulatorData.InfoMessage = 'FEATURE NOT SUPPORTED TOGGLE';
    if (aBoolean)
      $scope.SimulatorData.ErrorMessage = 'FEATURE NOT SUPPORTED DETECTED';
  }

  // FEATURE NOT SUPPORTED
  $scope.unkownKiosk = function (aBoolean) {
    let injectedURL = theRestIoServerUrl;
    injectedURL = injectedURL.replace('BHSCMD', 'SIMULATOR').replace('GETKIOSK', 'FORCEERROR');
    injectedURL += (aBoolean) ? '/6' : '/0';
    $http.get(injectedURL);
    $scope.SimulatorData.InfoMessage = 'UNKNOWN KIOSK TOGGLE';
    if (aBoolean)
      $scope.SimulatorData.ErrorMessage = 'UNKNOWN KIOSK DETECTED';
  }

  $scope.intrusion = function (kioskStatus) {
    let aBoolean = $scope.computeStatus(kioskStatus).intrusion;
    let injectedURL = theRestIoServerUrl;
    injectedURL = injectedURL.replace('BHSCMD', 'SIMULATOR').replace('GETKIOSK', 'INTRUSION');
    // TAKING OPPOSITE FOR TOGGLING
    injectedURL += (!aBoolean) ? '/1' : '/0';
    $http.get(injectedURL);
    $scope.SimulatorData.InfoMessage = ('INTRUSION TOGGLE');
    if (!aBoolean)
      $scope.SimulatorData.ErrorMessage = ('INTRUSION DETECTED');
  }

  $scope.computeStatus = function (kioskStatus) {
    let result = {};
    // kioskStatus %= 128;
    // if (kioskStatus %= 64 != kioskStatus);
    if (kioskStatus % 32 != kioskStatus) {
      kioskStatus %= 32;
      result.bhsReady = true;
    } else {
      result.bhsReady = false;
    }
    if (kioskStatus % 16 != kioskStatus) {
      kioskStatus %= 16;
      result.cellConveyor2 = true;
    } else {
      result.cellConveyor2 = false;
    }
    if (kioskStatus % 8 != kioskStatus) {
      kioskStatus %= 8;
    }
    if (kioskStatus % 4 != kioskStatus) {
      kioskStatus %= 4;
    }
    if (kioskStatus % 2 != kioskStatus) {
      kioskStatus %= 2;
      result.intrusion = true;
    } else {
      result.intrusion = false;
    }
    if (kioskStatus % 1 != kioskStatus) {
      kioskStatus %= 1;
      result.cellConveyor1 = true;
    } else {
      result.cellConveyor1 = false;
    }
    return result;
  }

  $scope.bagFiddle = function (travelerBagFiddle) {
    $scope.travelerBagFiddle = travelerBagFiddle;
    fiddleBag();
  }

  $scope.simulateError = function (errorNum) {
    console.log('inside simulateError:', errorNum);
    if (!Number(errorNum)) {
      let errorLabel = errorNum;
      for (key in $scope.errorList) {
        if ($scope.errorList[key] == errorLabel)
          errorNum = key;
      }
    }
    console.log('inside simulateError 2:', errorNum);
    console.log('Simulating Error with error code', errorNum);
    $http.get(["/simulateError", errorNum, $scope.kioskId].join('/'))
      .success(function (response) {
        console.log('Success sending error code for error ', errorNum, ' and kiosk: ', $scope.kioskId);
      })
      .error(function (error) {
        console.log('error sending error code for error ', errorNum, ' and kiosk: ', $scope.kioskId, ' error: ', error);
      });
  }

  $scope.simulateStatus = function (statusNum) {
    console.log('Simulating Status with Status code', statusNum);
    $http.get(["/simulateStatus", statusNum, $scope.kioskId].join('/'))
      .success(function (response) {
        console.log('Success sending Status code for Status ', statusNum, ' and kiosk: ', $scope.kioskId);
      })
      .error(function (error) {
        console.log('error sending status code for status ', statusNum, ' and kiosk: ', $scope.kioskId, ' error: ', error);
      });
  }

  $scope.simulateInfo = function (infoNum) {
    console.log('Simulating Info with info code', infoNum);
    $http.get(["/simulateInfo", infoNum, $scope.kioskId].join('/'))
      .success(function (response) {
        console.log('Success sending info code for info ', infoNum, ' and kiosk: ', $scope.kioskId);
      })
      .error(function (error) {
        console.log('error sending info code for info ', infoNum, ' and kiosk: ', $scope.kioskId, ' error: ', error);
      });
  }

  // $scope.sendSBDStatus() = function () {
  //   console.log('Simulating SBD with status info', $scope.SBDData);
  //   $http.get(["/simulateInfo", infoNum, $scope.kioskId].join('/'))
  //     .success(function (response) {
  //       console.log('Success sending info code for info ', infoNum, ' and kiosk: ', $scope.kioskId);
  //     })
  //     .error(function (error) {
  //       console.log('error sending info code for info ', infoNum, ' and kiosk: ', $scope.kioskId, ' error: ', error);
  //     });
  // }

  $scope.toggleSendScale = function (aBoolean) {
    $scope.sendScaleMessage = aBoolean;

    $http.get(['/sendScale', aBoolean, $scope.kioskId].join('/'))
      .success(function (response) {
        console.log('Success toggling scaleFeeder with new value ', aBoolean, ' and kiosk: ', $scope.kioskId);
      })
      .error(function (error) {
        console.log('error toggling scaleFeeder with new value ', aBoolean, ' and kiosk: ', $scope.kioskId, ' error: ', error);
      });
  }

  fiddleBag = function () {
    updateKioskWeight($scope.kiosk.CurrentWeight);
    if ($scope.travelerBagFiddle) {
      setTimeout(fiddleBag, 1000);
    }
  }

  $scope.toggleScaleThreeConveyorPosition = function () {
    if ($scope.scaleThreeConveyorPosition == 0)
      $scope.scaleThreeConveyorPosition = 1;
    else $scope.scaleThreeConveyorPosition = 0;
    $scope.adaptThreeConveyors();
  }

  $scope.postSBDStatus = function () {
    $http.post(['/sendSBDStatus', $scope.kioskId].join('/'), {
        sbdStatus: $scope.SBDData
      })
      .success(function (response) {
      })
      .error(function (error) {
        console.log("Failure sending SBD STATUS ", error);
      });
  }

  init();
  // $scope.refreshKioskData();
});