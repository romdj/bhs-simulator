const Stomp = require('stomp-client');


const amqcListener = new Stomp('10.0.0.40', 61613, 'admin', 'ictcs-s4amqc');
// let topic = '/STATUS';
let topic = '/topic/STATUS';
amqcListener.connect(function (sessionId) {
  amqcListener.subscribe(topic, (message, error) => {
    // console.log('message', message);
    publish('/topic/TESTBHSSimulator', message);
  });
});

function publish(queue, message){
  const amqcEmitter = new Stomp('10.0.0.40', 61613, 'admin', 'ictcs-s4amqc');
  amqcEmitter.connect(function (sessionId) {
    amqcEmitter.publish(queue, message);
    amqcEmitter.disconnect(()=>{
      console.log('disconnected from AMQC Client');
    })
  });
}
