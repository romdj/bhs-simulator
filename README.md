# README #

### What is this repository for? ###

* This repository aims to simulate a Kiosk environment in order to help the different parties involved to better represent what the environment will look on site.

* v 1.0.0
* path: https://bitbucket.org/romdj/bhs-simulator.git

### How do I get set up? ###

* The application is in 2 separate folders/workers:
	* RestAPI (written in Node.JS (ideally version >= 9.9)
	* Simulator (written in Angular.JS)
	
* To set up the environment, in both folders, enter the command `npm install`
* To start the RestAPI, enter within the RestAPI folder the command `node restioserver.js` or `nodemon restioserver.js`.
* Same for the Simulator, to run it, you must provide a config file inside the folder BHS-Simulator/config.
	Here's an example of such file:

```json
{
	"rest" : {
				"serverPort":8000
			},
	"RestIoServer" : {
						"url" :"http://127.0.0.1:6789/BHSCMD/GETKIOSK/"
				     },
	"bags" : [
	 		 {"LPC":"1111111111","Weight":11.1,"Image":"bag-brown.png","OverHeight":false,"OverLength":false},
			 {"LPC":"2222222222","Weight":22.2,"Image":"bag-blue.png","OverHeight":false,"OverLength":true},
			 {"LPC":"3333333333","Weight":33.3,"Image":"bag-yellow.png","OverHeight":true,"OverLength":false},
			 {"LPC":"4444444444","Weight":44.4,"Image":"bag-green.png","OverHeight":true,"OverLength":true}
			 ]
}
```
* This file had been saved under the following destination: BHS-Simulator/config/BHSSimulator.config

* To start the BHS Simulator enter inside the BHS-Simulator folder and provide the following command in order to start the service:
	* `node service.js -config=config/BHSSimulator.config` or 
	* `nodemon service.js -config=config/BHSSimulator.config` 
where the BHSSimulator.config
could be any file created that would work as a configuration file.


### Contribution guidelines ###

Feel free to place a Pull Request, the changes will be very much appreciated.

### Who do I talk to? ###

Romain Lussier
romain.lussier@external.engie.com

St�phane Corteel
stephane.corteel@engie.com

Engie Fabricom
